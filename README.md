![David Blue for iOS](https://i.snap.as/e0BjQx1L.png)

# David Blue's iHole

## A messy-ass collection of iOS-related configurations, documentation, assets, data, and automations.

* **"[David Blue on iPhone & iOS](https://github.com/extratone/bilge/wiki/David-Blue-on-iPhone-&-iOS)"** - *The Psalms* Wiki
* [**Index of the iOS Documentation section of my Notion account**](https://www.notion.so/rotund/iOS-Documentation-c56ec62d3c3140998d2b779dffff6ae1) (a complete mess, at the moment.)

If ya need help, *please feel free to contact me*. This link will take you to a handy contact card: **https://bit.ly/whoisdavidblue**.

⇨⇨⇨⇨[**iOS 15 Reviewed for My Family**](https://bilge.world/ios-15-family-review)

### Frames

* [**Shared iCloud Photos Album**](https://www.icloud.com/sharedalbum/#B0e5ON9t3GoUjSq)- [Shortlink](https://bit.ly/dbframes)

![help](https://user-images.githubusercontent.com/43663476/157653415-c038e57e-6e7e-4315-8346-1fbf56eb0b30.png)

## System

* [Geekbench profile](https://browser.geekbench.com/user/238089)

## Siri Shortcuts

* [**RoutineHub profile**](https://routinehub.co/user/blue)
* [ShareShortcuts profile](https://shareshortcuts.com/u/blue/)
* [**Siri Shortcuts Raindrop Collection**](https://raindrop.io/davidblue/siri-shortcuts-21598130)

## Apps

* **Currently Installed Apps** ([Excel Spreadsheet](https://github.com/extratone/i/blob/main/apps.xlsx))
* [**Homescreen Configuration**](http://bit.ly/dbhomescreen)

## Video

* [iOS & iPhone YouTube Playlist](https://youtube.com/playlist?list=PLC1ONWRxWOkvPjHxX9pNkT1N_PbVLvSLh)

## Images

* [**Shared iCloud Photos Wallpapers Album**](https://bit.ly/dbiosbg) 
* [**iPhone 12 Pro Max main gallery**](https://snap.as/extratone/iphone-12-pro-max)
* [iPhone 12 Pro Max Flickr album](https://flic.kr/s/aHsmTiT46W)
* [iPhone 8 Plus Flickr album](https://flic.kr/s/aHsm3KioDG)
* [iPhone 4 Flickr album](https://flic.kr/s/aHsm6rcxN3)

## Audio

* [***End User*'s Siri Shortcuts episode**](https://anchor.fm/davidblue/episodes/Siri-Shortcuts-Are-iOS-12s-Most-Profound-Addition-e2eepe)
* (A handy ready-to-convert library of Garageband files for ringtones and notification sounds is next on my todo list.)

***
# Master App List
Maintained in a GitHub Gist.

[**Shorttlink**](https://bit.ly/dbapplist) - `https://bit.ly/dbapplist`

## An ongoing list of favorite, desired, noteworthy, absurd, despicable, scammy, etc. iOS App Store links.

Thanks to the **absolutely invaluable** [AppWish app,](https://apps.apple.com/us/app/appwish-simple-wishlist/id1291909819), this list is _much_ easier to maintain.

* [‎Adaptivity (A) on the App Store](https://itunes.apple.com/us/app/id1054670022)
* [‎Age Clock on the App Store](https://itunes.apple.com/us/app/id1152838744)
* [‎Airmail - Gmail Outlook Mail on the App Store](https://itunes.apple.com/us/app/id993160329)
* [‎Airtable on the App Store](https://itunes.apple.com/us/app/id914172636)
* [‎AlertsBoard on the App Store](https://itunes.apple.com/us/app/id1513341683)
* [‎Alpenglow: Sunset Forecasts on the App Store](https://itunes.apple.com/us/app/id978589174)
* [‎Amway™ on the App Store](https://itunes.apple.com/us/app/id1485234014)
* [‎Annotable: Annotation &amp; Markup on the App Store](https://itunes.apple.com/us/app/id1099850421)
* [‎Anybuffer on the App Store](https://itunes.apple.com/us/app/id1330815414)
* [‎AppWish - Simple wishlist on the App Store](https://itunes.apple.com/us/app/id1291909819)
* [‎Audio Memos SE on the App Store](https://itunes.apple.com/us/app/id304075033)
* [‎Aviary on the App Store](https://itunes.apple.com/us/app/id1522043420)
* [‎B4X for Pleroma &amp; Mastodon on the App Store](https://itunes.apple.com/us/app/id1538396871)
* [‎BARS - Rapper's Delight on the App Store](https://itunes.apple.com/us/app/id1521789321)
* [‎Be My Eyes on the App Store](https://itunes.apple.com/us/app/id905177575)
* [‎Beams: Social Micro Podcasts on the App Store](https://itunes.apple.com/us/app/id1499706193)
* [‎BestPhotos - Library Manager on the App Store](https://itunes.apple.com/us/app/id1168605247)
* [‎Bible Gateway on the App Store](https://itunes.apple.com/us/app/id506512797)
* [‎BitLife - Life Simulator on the App Store](https://itunes.apple.com/us/app/id1374403536)
* [‎Blind - Workplace Community on the App Store](https://itunes.apple.com/us/app/id737534965)
* [‎Blink Shell: Mosh &amp; SSH Client on the App Store](https://itunes.apple.com/us/app/id1156707581)
* [‎Bluraer on the App Store](https://itunes.apple.com/us/app/id1515357421)
* [‎Book Track - Library Manager on the App Store](https://itunes.apple.com/us/app/id1491660771)
* [‎Brief - Just the news you need on the App Store](https://itunes.apple.com/us/app/id1475186118)
* [‎Bus Simulator : Ultimate on the App Store](https://itunes.apple.com/us/app/id1461749632)
* [‎CARROT Weather on the App Store](https://itunes.apple.com/us/app/id961390574)
* [‎CPU-x Dasher z Battery life on the App Store](https://itunes.apple.com/us/app/id1359763765)
* [‎Cachly - Geocaching on the App Store](https://itunes.apple.com/us/app/id645384141)
* [‎Cake Web Browser on the App Store](https://itunes.apple.com/us/app/id1163553130)
* [‎Calca on the App Store](https://itunes.apple.com/us/app/id635757879)
* [‎Car Parking Multiplayer on the App Store](https://itunes.apple.com/us/app/id1374868881)
* [‎Cardhop on the App Store](https://itunes.apple.com/us/app/id1448744070)
* [‎Celestia - Space Simulator on the App Store](https://itunes.apple.com/us/app/id1500434829)
* [‎Cepher on the App Store](https://itunes.apple.com/us/app/id1111754263)
* [‎Charty for Shortcuts on the App Store](https://itunes.apple.com/us/app/id1494386093)
* [‎Cinnamon Video on the App Store](https://itunes.apple.com/us/app/id1531708639)
* [‎Circio IRC on the App Store](https://itunes.apple.com/us/app/id1538460130)
* [‎Cleora on the App Store](https://itunes.apple.com/us/app/id1497821000)
* [‎Codecademy Go on the App Store](https://itunes.apple.com/us/app/id1376029326)
* [‎Collab - Music Video Editor on the App Store](https://itunes.apple.com/us/app/id1148897356)
* [‎Collected Notes on the App Store](https://itunes.apple.com/us/app/id1511343960)
* [‎Colloquy - IRC Client on the App Store](https://itunes.apple.com/us/app/id302000478)
* [‎Color Pro – P3 Picker on the App Store](https://itunes.apple.com/us/app/id1207928528)
* [‎Command Browser on the App Store](https://itunes.apple.com/us/app/id1485289520)
* [‎Company of Heroes on the App Store](https://itunes.apple.com/us/app/id1464645812)
* [‎Control Orienteering Analysis on the App Store](https://itunes.apple.com/us/app/id1329182939)
* [‎Crew Questions on the App Store](https://itunes.apple.com/us/app/id1547171709)
* [‎Crowd City on the App Store](https://itunes.apple.com/us/app/id1444062497)
* [‎Curator - visual notes on the App Store](https://itunes.apple.com/us/app/id593195406)
* [‎DataGist on the App Store](https://itunes.apple.com/us/app/id1234159075)
* [‎Death Road to Canada on the App Store](https://itunes.apple.com/us/app/id1128464707)
* [‎Debug Anywhere on the App Store](https://itunes.apple.com/us/app/id1555924269)
* [‎Deck - Flashcard Learning App on the App Store](https://itunes.apple.com/us/app/id1545176753)
* [‎Deliveries: a package tracker on the App Store](https://itunes.apple.com/us/app/id290986013)
* [‎Denim - Playlist Cover Maker on the App Store](https://itunes.apple.com/us/app/id1532250420)
* [‎Design Home: House Makeover on the App Store](https://itunes.apple.com/us/app/id1010962391)
* [‎Design+Code on the App Store](https://itunes.apple.com/us/app/id1281776514)
* [‎Destruction simulator on the App Store](https://itunes.apple.com/us/app/id1532414772)
* [‎DetailsPro on the App Store](https://itunes.apple.com/us/app/id1524366536)
* [‎Device Info Toolkit on the App Store](https://itunes.apple.com/us/app/id1529223828)
* [‎Dialup: Reviving the Phone on the App Store](https://itunes.apple.com/us/app/id1295012267)
* [‎Dispo - Live in the Moment on the App Store](https://itunes.apple.com/us/app/id1491684197)
* [‎Doppler MP3 &amp; FLAC Player on the App Store](https://itunes.apple.com/us/app/id1468459747)
* [‎DraftCode Offline PHP IDE on the App Store](https://itunes.apple.com/us/app/id593757593)
* [‎Drafts on the App Store](https://itunes.apple.com/us/app/id1236254471)
* [‎Driving School 3D Simulator on the App Store](https://itunes.apple.com/us/app/id1238658080)
* [‎Duet Display on the App Store](https://itunes.apple.com/us/app/id935754064)
* [‎Ecosia on the App Store](https://itunes.apple.com/us/app/id670881887)
* [‎Elytra on the App Store](https://itunes.apple.com/us/app/id1433266971)
* [‎Email Client - Boomerang Mail on the App Store](https://itunes.apple.com/us/app/id1236906322)
* [‎Emoji for Message - Text Maker on the App Store](https://itunes.apple.com/us/app/id541879701)
* [‎Essayist - APA &amp; MLA Essays on the App Store](https://itunes.apple.com/us/app/id1537845384)
* [‎Everlog on the App Store](https://itunes.apple.com/us/app/id1519935634)
* [‎Exif Metadata on the App Store](https://itunes.apple.com/us/app/id1455197364)
* [‎Extreme Car Driving Simulator on the App Store](https://itunes.apple.com/us/app/id959498315)
* [‎FANDOM News &amp; Stories on the App Store](https://itunes.apple.com/us/app/id1230063803)
* [‎Facebook Ads Manager on the App Store](https://itunes.apple.com/us/app/id964397083)
* [‎FanFiction.Net on the App Store](https://itunes.apple.com/us/app/id1192753879)
* [‎Fedi for Pleroma and Mastodon on the App Store](https://itunes.apple.com/us/app/id1478806281)
* [‎Femto Editor on the App Store](https://itunes.apple.com/us/app/id1482210825)
* [‎Fenix for Twitter on the App Store](https://itunes.apple.com/us/app/id1437821840)
* [‎Ferrite Recording Studio on the App Store](https://itunes.apple.com/us/app/id1018780185)
* [‎Fiery Feeds: RSS Reader on the App Store](https://itunes.apple.com/us/app/id1158763303)
* [‎Fig for Discourse on the App Store](https://itunes.apple.com/us/app/id1485491193)
* [‎Final Draft Mobile on the App Store](https://itunes.apple.com/us/app/id526135686)
* [‎Find IP • Public &amp; WiFi on the App Store](https://itunes.apple.com/us/app/id1468108412)
* [‎Find: Command+F for Camera on the App Store](https://itunes.apple.com/us/app/id1506500202)
* [‎Flutter UI Templates on the App Store](https://itunes.apple.com/us/app/id1524943343)
* [‎Fontise - Font Maker Keyboard on the App Store](https://itunes.apple.com/us/app/id1095756216)
* [‎For All Mankind: Time Capsule on the App Store](https://itunes.apple.com/us/app/id1541425599)
* [‎GPS Tracks on the App Store](https://itunes.apple.com/us/app/id425589565)
* [‎GPX-Viewer on the App Store](https://itunes.apple.com/us/app/id880510678)
* [‎GameTrack on the App Store](https://itunes.apple.com/us/app/id1136800740)
* [‎GarageBand on the App Store](https://itunes.apple.com/us/app/id408709785)
* [‎Geocaching Datalogger on the App Store](https://itunes.apple.com/us/app/id1525850620)
* [‎Geometry solver ² - calculator on the App Store](https://itunes.apple.com/us/app/id1226352729)
* [‎GitJournal on the App Store](https://itunes.apple.com/us/app/id1466519634)
* [‎Golang Recipes on the App Store](https://itunes.apple.com/us/app/id1452119947)
* [‎Golden Quran | المصحف الذهبي on the App Store](https://itunes.apple.com/us/app/id852497554)
* [‎GoodLinks on the App Store](https://itunes.apple.com/us/app/id1474335294)
* [‎GoodTask - To Do List, Tasks on the App Store](https://itunes.apple.com/us/app/id1068039220)
* [‎Google Duo on the App Store](https://itunes.apple.com/us/app/id1096918571)
* [‎HEY Email on the App Store](https://itunes.apple.com/us/app/id1506603805)
* [‎HTTP requests on the App Store](https://itunes.apple.com/us/app/id1492817276)
* [‎Halide Mark II - Pro Camera on the App Store](https://itunes.apple.com/us/app/id885697368)
* [‎Happy Printer on the App Store](https://itunes.apple.com/us/app/id1539388451)
* [‎Hashtag Editor on the App Store](https://itunes.apple.com/us/app/id1554772749)
* [‎Honk on the App Store](https://itunes.apple.com/us/app/id1458452703)
* [‎Hopscotch-Programming for kids on the App Store](https://itunes.apple.com/us/app/id617098629)
* [‎Hyper School on the App Store](https://itunes.apple.com/us/app/id1512017896)
* [‎Hyphen. on the App Store](https://itunes.apple.com/us/app/id1024204679)
* [‎Iconboard on the App Store](https://itunes.apple.com/us/app/id1535254664)
* [‎Idle Ants - Simulator Game on the App Store](https://itunes.apple.com/us/app/id1529310183)
* [‎If Found... on the App Store](https://itunes.apple.com/us/app/id1440072561)
* [‎Illustail on the App Store](https://itunes.apple.com/us/app/id375749531)
* [‎Infinite Flight Simulator on the App Store](https://itunes.apple.com/us/app/id471341991)
* [‎Is It Down? – Uptime Checker Widget on the App Store](https://itunes.apple.com/us/app/id921001850)
* [‎Jellycuts on the App Store](https://itunes.apple.com/us/app/id1522625245)
* [‎Just Timers on the App Store](https://itunes.apple.com/us/app/id1453573845)
* [‎Kodex on the App Store](https://itunes.apple.com/us/app/id1038574481)
* [‎Kodika - No Code App Builder on the App Store](https://itunes.apple.com/us/app/id1433898888)
* [‎Landscape: Mountaineering on the App Store](https://itunes.apple.com/us/app/id1500216643)
* [‎Launcher with Multiple Widgets on the App Store](https://itunes.apple.com/us/app/id905099592)
* [‎Learn Math Facts on the App Store](https://itunes.apple.com/us/app/id1485006919)
* [‎LibTerm on the App Store](https://itunes.apple.com/us/app/id1380911705)
* [‎LimeChat - IRC Client on the App Store](https://itunes.apple.com/us/app/id298766460)
* [‎Linky for Twitter and Mastodon on the App Store](https://itunes.apple.com/us/app/id438090426)
* [‎Linux Command manual on the App Store](https://itunes.apple.com/us/app/id1356504527)
* [‎LiveATC Air Radio on the App Store](https://itunes.apple.com/us/app/id317809458)
* [‎Living Earth - Clock &amp; Weather on the App Store](https://itunes.apple.com/us/app/id379869627)
* [‎Longplay on the App Store](https://itunes.apple.com/us/app/id1495152002)
* [‎Lorem Ipsum Text on the App Store](https://itunes.apple.com/us/app/id1521103770)
* [‎Lowriders Comeback 2: Cruising on the App Store](https://itunes.apple.com/us/app/id1291730054)
* [‎Lua IDE on the App Store](https://itunes.apple.com/us/app/id1549382090)
* [‎Mapaper on the App Store](https://itunes.apple.com/us/app/id1546487705)
* [‎Mario Kart Tour on the App Store](https://itunes.apple.com/us/app/id1293634699)
* [‎Markdown Tables on the App Store](https://itunes.apple.com/us/app/id1476068521)
* [‎Markdown゜ on the App Store](https://itunes.apple.com/us/app/id1494902837)
* [‎McClockface on the App Store](https://itunes.apple.com/us/app/id1544343485)
* [‎Meeter for Zoom, Teams &amp; Co on the App Store](https://itunes.apple.com/us/app/id1510445899)
* [‎Member Tools on the App Store](https://itunes.apple.com/us/app/id391093033)
* [‎Mercury for Mastodon on the App Store](https://itunes.apple.com/us/app/id1486749200)
* [‎Metapho on the App Store](https://itunes.apple.com/us/app/id914457352)
* [‎Micro.blog on the App Store](https://itunes.apple.com/us/app/id1253201335)
* [‎Microsoft Lists on the App Store](https://itunes.apple.com/us/app/id1530637363)
* [‎Microsoft Pix on the App Store](https://itunes.apple.com/us/app/id1127910488)
* [‎Microsoft SwiftKey Keyboard on the App Store](https://itunes.apple.com/us/app/id911813648)
* [‎MindNode - Mind Map &amp; Outline on the App Store](https://itunes.apple.com/us/app/id1218718027)
* [‎MissCat - Misskey クライアント - on the App Store](https://itunes.apple.com/us/app/id1505059993)
* [‎MixEffect on the App Store](https://itunes.apple.com/us/app/id1555812675)
* [‎Musens on the App Store](https://itunes.apple.com/us/app/id1560526260)
* [‎Museum Alive on the App Store](https://itunes.apple.com/us/app/id1548503337)
* [‎Music Info — Song Metadata on the App Store](https://itunes.apple.com/us/app/id1521580618)
* [‎MusicHarbor - Track New Music on the App Store](https://itunes.apple.com/us/app/id1440405750)
* [‎NOISE on the App Store](https://itunes.apple.com/us/app/id1011132019)
* [‎Need for Speed No Limits on the App Store](https://itunes.apple.com/us/app/id883393043)
* [‎Neutron Music Player on the App Store](https://itunes.apple.com/us/app/id766858884)
* [‎Nighthawk for Twitter on the App Store](https://itunes.apple.com/us/app/id1481777438)
* [‎Ninja Attack! on the App Store](https://itunes.apple.com/us/app/id1095039966)
* [‎Notes Plus on the App Store](https://itunes.apple.com/us/app/id374211477)
* [‎Noto - Elegant Note on the App Store](https://itunes.apple.com/us/app/id1459055246)
* [‎OneLink Simulator on the App Store](https://itunes.apple.com/us/app/id1550796743)
* [‎Open GPX Tracker on the App Store](https://itunes.apple.com/us/app/id984503772)
* [‎Opensignal Internet Speed Test on the App Store](https://itunes.apple.com/us/app/id598298030)
* [‎OptiFlight on the App Store](https://itunes.apple.com/us/app/id1490464625)
* [‎Otter: Transcribe Voice Notes on the App Store](https://itunes.apple.com/us/app/id1276437113)
* [‎Oyakodon for Mastodon on the App Store](https://itunes.apple.com/us/app/id1229174544)
* [‎PANTONE Studio on the App Store](https://itunes.apple.com/us/app/id329515634)
* [‎Papers, Please on the App Store](https://itunes.apple.com/us/app/id935216956)
* [‎Pineapple - Build Apps on the App Store](https://itunes.apple.com/us/app/id1443967643)
* [‎Pixel Art Camera on the App Store](https://itunes.apple.com/us/app/id1107180652)
* [‎PlayTally: Apple Music Stats on the App Store](https://itunes.apple.com/us/app/id1513271356)
* [‎Pocket Casts on the App Store](https://itunes.apple.com/us/app/id414834813)
* [‎Pocket Palette on the App Store](https://itunes.apple.com/us/app/id1342063329)
* [‎Podcast Player - OwlTail on the App Store](https://itunes.apple.com/us/app/id1447809682)
* [‎Post-it® on the App Store](https://itunes.apple.com/us/app/id920127738)
* [‎Pretext on the App Store](https://itunes.apple.com/us/app/id1347707000)
* [‎Prison Life! on the App Store](https://itunes.apple.com/us/app/id1540913781)
* [‎Project CARS GO on the App Store](https://itunes.apple.com/us/app/id1418668113)
* [‎Psiphon on the App Store](https://itunes.apple.com/us/app/id1276263909)
* [‎Punkt: One-sentence journal on the App Store](https://itunes.apple.com/us/app/id1507575828)
* [‎Puppr - Dog Training &amp; Tricks on the App Store](https://itunes.apple.com/us/app/id1199338956)
* [‎Pushlog - Server Logging on the App Store](https://itunes.apple.com/us/app/id1522516018)
* [‎QuickNotes App on the App Store](https://itunes.apple.com/us/app/id1535373827)
* [‎Rave – Watch Party on the App Store](https://itunes.apple.com/us/app/id929775122)
* [‎React Native Bootstrap Styles on the App Store](https://itunes.apple.com/us/app/id1535407626)
* [‎Read Scripture on the App Store](https://itunes.apple.com/us/app/id1067865974)
* [‎ReadKit - Read later and RSS on the App Store](https://itunes.apple.com/us/app/id1544603988)
* [‎Readwise on the App Store](https://itunes.apple.com/us/app/id1476885528)
* [‎Reeder 5 on the App Store](https://itunes.apple.com/us/app/id1529445840)
* [‎Repeat - Habits &amp; Wellbeing on the App Store](https://itunes.apple.com/us/app/id1546204928)
* [‎RoadWarrior Route Planner on the App Store](https://itunes.apple.com/us/app/id959621658)
* [‎Roma for Pleroma and Mastodon on the App Store](https://itunes.apple.com/us/app/id1445328699)
* [‎RuneScape on the App Store](https://itunes.apple.com/us/app/id1332022656)
* [‎Rusty Lake Hotel on the App Store](https://itunes.apple.com/us/app/id1059911569)
* [‎SanDisk iXpand™ Drive on the App Store](https://itunes.apple.com/us/app/id1079870681)
* [‎Scan Thing: Scan Anything on the App Store](https://itunes.apple.com/us/app/id1542730895)
* [‎Scarlet on the App Store](https://itunes.apple.com/us/app/id1532669250)
* [‎Schooly on the App Store](https://itunes.apple.com/us/app/id1524435833)
* [‎Scrivener on the App Store](https://itunes.apple.com/us/app/id972387337)
* [‎Sedona - Compile Swift Program on the App Store](https://itunes.apple.com/us/app/id1155835459)
* [‎SeeLess - C Compiler on the App Store](https://itunes.apple.com/us/app/id1481018071)
* [‎ServerCat - Linux Status &amp; SSH on the App Store](https://itunes.apple.com/us/app/id1501532023)
* [‎Ship: Dating &amp; Matchmaking App on the App Store](https://itunes.apple.com/us/app/id1448104758)
* [‎Sling Plane 3D on the App Store](https://itunes.apple.com/us/app/id1553278236)
* [‎Snapfish: Photo Books &amp; Cards on the App Store](https://itunes.apple.com/us/app/id330035194)
* [‎Snapseed on the App Store](https://itunes.apple.com/us/app/id439438619)
* [‎SocialHub - SocialMedia Client on the App Store](https://itunes.apple.com/us/app/id1474451582)
* [‎Socratic by Google on the App Store](https://itunes.apple.com/us/app/id1014164514)
* [‎Sofa: Downtime Organizer on the App Store](https://itunes.apple.com/us/app/id1276554886)
* [‎Soor ▹ on the App Store](https://itunes.apple.com/us/app/id1439731526)
* [‎Soundtrap Studio on the App Store](https://itunes.apple.com/us/app/id991031323)
* [‎Spaceteam on the App Store](https://itunes.apple.com/us/app/id570510529)
* [‎Spectre Camera on the App Store](https://itunes.apple.com/us/app/id1450074595)
* [‎Speechify - Audio Text Reader on the App Store](https://itunes.apple.com/us/app/id1209815023)
* [‎Speed 5 on the App Store](https://itunes.apple.com/us/app/id495584389)
* [‎Speedify on the App Store](https://itunes.apple.com/us/app/id999025824)
* [‎Splash - Programming Language on the App Store](https://itunes.apple.com/us/app/id1455793030)
* [‎Spotify Greenroom: Talk live on the App Store](https://itunes.apple.com/us/app/id1517524960)
* [‎Spotify for Artists on the App Store](https://itunes.apple.com/us/app/id1222021797)
* [‎State.io - Conquer the World on the App Store](https://itunes.apple.com/us/app/id1559032748)
* [‎Stereo: Join the conversation on the App Store](https://itunes.apple.com/us/app/id1501818976)
* [‎Streaks on the App Store](https://itunes.apple.com/us/app/id963034692)
* [‎Subtrack: Track Subscriptions on the App Store](https://itunes.apple.com/us/app/id1519946553)
* [‎Sundial Solar &amp; Lunar Time on the App Store](https://itunes.apple.com/us/app/id976460540)
* [‎Supersonic News: World &amp; Local on the App Store](https://itunes.apple.com/us/app/id1550224300)
* [‎Surge 4 on the App Store](https://itunes.apple.com/us/app/id1442620678)
* [‎Swimbols on the App Store](https://itunes.apple.com/us/app/id1525226399)
* [‎Syndromi on the App Store](https://itunes.apple.com/us/app/id1547850133)
* [‎Tailor - Screenshot Stitching on the App Store](https://itunes.apple.com/us/app/id926653095)
* [‎TapCoding on the App Store](https://itunes.apple.com/us/app/id1106047716)
* [‎Terminology Dictionary on the App Store](https://itunes.apple.com/us/app/id687798859)
* [‎TestApp.io on the App Store](https://itunes.apple.com/us/app/id1518972541)
* [‎TestM - Check phone &amp; Report on the App Store](https://itunes.apple.com/us/app/id1242371446)
* [‎Text Case on the App Store](https://itunes.apple.com/us/app/id1407730596)
* [‎Text Split on the App Store](https://itunes.apple.com/us/app/id1547206241)
* [‎Textcraft on the App Store](https://itunes.apple.com/us/app/id1546719359)
* [‎The CW on the App Store](https://itunes.apple.com/us/app/id491730359)
* [‎The Chronos Principle on the App Store](https://itunes.apple.com/us/app/id1551295225)
* [‎The Wallpaper &amp; Background App on the App Store](https://itunes.apple.com/us/app/id1518228413)
* [‎This War of Mine on the App Store](https://itunes.apple.com/us/app/id982175678)
* [‎Time Zone Pro on the App Store](https://itunes.apple.com/us/app/id1557558517)
* [‎Timery for Toggl on the App Store](https://itunes.apple.com/us/app/id1425368544)
* [‎Timewave on the App Store](https://itunes.apple.com/us/app/id1524345488)
* [‎Toolbox for Pages on the App Store](https://itunes.apple.com/us/app/id595343713)
* [‎TouchTone™ on the App Store](https://itunes.apple.com/us/app/id962133182)
* [‎True Skate on the App Store](https://itunes.apple.com/us/app/id549105915)
* [‎Used Cars Dealer-Car Sale Game on the App Store](https://itunes.apple.com/us/app/id1548247708)
* [‎Wonder: Browser for Wikipedia on the App Store](https://itunes.apple.com/us/app/id1050888989)

(The list below is deprecated but still worthwhile.)
```
# Most Intriguing iOS Apps

## An ongoing list of links to iOS apps, App Stories, and greater media coverage.

I’ve privately maintained a list like this for as long as I can remember, to be honest, but it’s never been particularly useful to others until now. Though it’ll probably never be alphabatized, standardized, or comprehensively annotated, it *will* be kept relatively up-to-date - certainly for the duration of my iPhone 12 Pro Max long review.

## Apps

* [‎Mapaper on the App Store](https://apps.apple.com/us/app/mapaper/id1546487705)
* [‎Yahoo Mail - Organized Email on the App Store](https://apps.apple.com/us/app/yahoo-mail-organized--email/id577586159)
* [‎Socratic by Google on the App Store](https://apps.apple.com/us/app/socratic-by-google/id1014164514)
* [‎The CW on the App Store](https://apps.apple.com/us/app/the-cw/id491730359)
* [‎Ninja Attack! on the App Store](https://apps.apple.com/us/app/ninja-attack/id1095039966)
* [‎TTYL on the App Store](https://apps.apple.com/us/app/ttyl/id1422442907)
* [‎Honk on the App Store](https://apps.apple.com/us/app/honk/id1458452703)
* [‎Destruction simulator on the App Store](https://apps.apple.com/us/app/destruction-simulator/id1532414772)
* [‎Aerofly FS 2021 on the App Store](https://apps.apple.com/us/app/aerofly-fs-2021/id1539931689)
* [‎Cake Web Browser on the App Store](https://apps.apple.com/us/app/cake-web-browser/id1163553130)
* [‎Nighthawk for Twitter on the App Store](https://apps.apple.com/us/app/nighthawk-for-twitter/id1481777438)
* [‎Psiphon on the App Store](https://apps.apple.com/us/app/psiphon/id1276263909)
* [‎Microsoft Lists on the App Store](https://apps.apple.com/us/app/microsoft-lists/id1530637363)
* [‎Microsoft Pix on the App Store](https://apps.apple.com/us/app/microsoft-pix/id1127910488)
* [‎Spectre Camera on the App Store](https://apps.apple.com/us/app/spectre-camera/id1450074595)
* [‎Fenix for Twitter on the App Store](https://apps.apple.com/us/app/fenix-for-twitter/id1437821840)
* [‎Toolbox for Pages on the App Store](https://apps.apple.com/us/app/toolbox-for-pages/id595343713)
* [‎HEY Email on the App Store](https://apps.apple.com/us/app/hey-email/id1506603805)
* [‎Aviary on the App Store](https://apps.apple.com/us/app/aviary/id1522043420)
* [Scrivener on the App Store](https://apps.apple.com/us/app/scrivener/ id972387337)
* [‎Final Draft Mobile on the App Store](https://apps.apple.com/us/app/final-draft-mobile/id526135686)
* [‎FiLMiC Pro Remote Control Camera Kit on the App Store](https://apps.apple.com/us/app-bundle/filmic-pro-remote-control-camera-kit/id1144188140)
* [‎Age Clock on the App Store](https://apps.apple.com/us/app/age-clock/id1152838744)
* [‎How Swift Powered 3 Extraordinary Apps : Mac App Store Story](https://apps.apple.com/us/story/id1526895453)
* [‎BARS - Rapper's Delight on the App Store](https://apps.apple.com/us/app/bars-rappers-delight/id1521789321)
* [‎E.gg - digital zine creator on the App Store](https://apps.apple.com/us/app/e-gg-digital-zine-creator/id1506631189)
* [‎Snapseed on the App Store](https://apps.apple.com/us/app/snapseed/id439438619)
* [‎Reeder 5 on the App Store](https://apps.apple.com/us/app/reeder-5/id1529445840)
* [‎Dispo - Live in the Moment on the App Store](https://apps.apple.com/us/app/dispo-live-in-the-moment/id1491684197)
* [‎NOISE on the App Store](https://apps.apple.com/us/app/noise/id1011132019)
* [‎Music Makers : Mac App Store Story](https://apps.apple.com/us/story/id1543999208)
* [‎A News App for the Curious : App Store Story](https://apps.apple.com/us/story/id1552768993)
* [‎Ferrite Recording Studio on the App Store](https://apps.apple.com/us/app/ferrite-recording-studio/id1018780185)
* [‎Living Earth - Clock & Weather on the App Store](https://apps.apple.com/us/app/living-earth-clock-weather/id379869627)
* [‎V for Wikipedia on the App Store](https://apps.apple.com/us/app/v-for-wikipedia/id993435362)
* [‎iziCast on the App Store](https://apps.apple.com/us/app/izicast/id1462571191)
* [‎Carrot Weather : App Store Story](https://apps.apple.com/us/story/id1470366384)
* [‎Annotable: Annotation & Markup on the App Store](https://apps.apple.com/us/app/annotable-annotation-markup/id1099850421)
* [‎PANTONE Studio on the App Store](https://apps.apple.com/us/app/pantone-studio/id329515634)
* [‎GitJournal on the App Store](https://apps.apple.com/us/app/gitjournal/id1466519634)
* [‎For All Mankind: Time Capsule on the App Store](https://apps.apple.com/us/app/for-all-mankind-time-capsule/id1541425599)
* [‎Cardhop on the App Store](https://apps.apple.com/us/app/cardhop/id1448744070)
* [‎Otter Voice Meeting Notes on the App Store](https://apps.apple.com/us/app/otter-voice-meeting-notes/id1276437113)
* [‎Telescope: News Reader on the App Store](https://apps.apple.com/us/app/telescope-news-reader/id1353282977)
* [‎Fontise - Font Maker Keyboard on the App Store](https://apps.apple.com/us/app/fontise-font-maker-keyboard/id1095756216)

## App Stories

* [‎Stream Bieber Live in TikTok : App Store Story](https://apps.apple.com/us/story/id1553564750)
* [‎Capsicum : App Store Story](https://apps.apple.com/us/story/id1550829986)
* [‎Finance for Couples : App Store Story](https://apps.apple.com/us/story/id1298301514)
* [‎Radiant One : App Store Story](https://apps.apple.com/us/story/id1441481123)
* [‎Mend a Broken Heart : App Store Story](https://apps.apple.com/us/story/id1341692115)
* [‎Champion Black Developers : App Store Story](https://apps.apple.com/us/story/id1547708486)
* [‎Drafts : App Store Story](https://apps.apple.com/us/story/id1444931250)
* "[Bringing CARROT Weather to Apple Watch](https://developer.apple.com/app-store/grailr/)" | App Store Strory(?)
* Video: "[Bringing CARROT Weather to Apple Watch](https://developer.apple.com/videos/play/insights/103)" | *Apple Developer*

## Apps in Tech Media

* [Apple’s App Store is hosting multimillion-dollar scams, says this iOS developer](https://www.theverge.com/2021/2/8/22272849/apple-app-store-scams-ios-fraud-reviews-ratings-flicktype) | *The Verge*
* [The Making of the CARROT Apps with Brian Mueller](https://appstories.net/episodes/6/) | *AppStories* Episode 6

# iOS Apps to Watch

**Note:** *I basically just copied this from a note that hadn't been modified in years. Suffice it to say, you should probably check versioning dates before downloading any of them.*

* [Typography Insight for iPhone](https://itunes.apple.com/us/app/typography-insight-for-iphone/id516501242?mt=8)
* [Google Duo](https://itunes.apple.com/us/app/google-duo-video-calling/id1096918571?mt=8)
* [LiveATC Air Radio](https://itunes.apple.com/us/app/liveatc-air-radio/id317809458?mt=8)
* [Halide Camera](https://itunes.apple.com/us/app/halide-camera/id885697368?mt=8)
* [Airmail](https://itunes.apple.com/us/app/airmail-your-mail-with-you/id993160329?mt=8)
* [Anybuffer](https://itunes.apple.com/us/app/anybuffer/id1330815414?mt=8) 
* [Metapho](https://itunes.apple.com/us/app/metapho/id914457352?mt=8)
* [Blind](https://itunes.apple.com/us/app/blind-anonymous-work-talk/id737534965?mt=8)
* [inRoute Route Planner](https://itunes.apple.com/us/app/inroute-route-planner/id703796787?mt=8)
* [Infinite Flight](https://itunes.apple.com/us/app/infinite-flight/id471341991?mt=8)
* [Collab](https://apps.apple.com/us/app/collab-music-video-editor/id1148897356)
* [‎Ship](https://apps.apple.com/us/app/ship-dating-made-fun-again/id1448104758)
* [RoadWarrior Route Planner](https://apps.apple.com/us/app/roadwarrior-route-planner/id959621658)
* [Punkt](https://apps.apple.com/us/app/punkt-one-sentence-journal/id1507575828)
* [theSkimm](https://apps.apple.com/us/app/theskimm/id1034995058)
* [Noto](https://apps.apple.com/us/app/noto-elegant-note/id1459055246)
* [FinalDraft Mobile](https://apps.apple.com/us/app/final-draft-mobile/id526135686)
* [Airtable](https://apps.apple.com/us/app/airtable/id914172636)
* [MindNode](https://apps.apple.com/us/app/mindnode-mind-map/id1218718027)
* [Spike](https://apps.apple.com/us/story/id1521340367)
```

